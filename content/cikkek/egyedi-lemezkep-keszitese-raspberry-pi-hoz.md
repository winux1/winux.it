---
title: "Egyedi lemezkép készítése Raspberry Pi-hoz"
date: 2020-10-29T10:28:49+01:00
draft: false
tags: [ "Hogyan használd" ]
image: images/Raspberry-Pi-Backup-Banner.png
---

Ha egyszerre több Raspberry Pi-ra szeretnénk felrakni ugyanazokat a programokat, akkor célszerű készítenünk egy egyedi lemezképet, amit már eleve úgy állítunk be, ahogyan azt mi szeretnénk, ezzel is gyorsabbá téve Pi-jaink üzembehelyezését.
<!--more-->
Először is fogjunk egy gyári lemezképet, és konfiguráljuk úgy, mint ahogyan normálisan szoktuk. A konfigurálás után helyezzük vissza az SD-kártyát a számítógépünkbe, majd a `sudo dd if=/dev/sd-kártya of=lemezkép.img status=progress` paranccsal készítsünk mentést az SD-kártyáról. A mentés elkészítése az SD-kártya nagyságától függően több órát is igénybe vehet, és a kész mentés mérete meg fog egyezni az SD-kártya méretével.

Akár már így is használhatnánk a lemezképet, de ajánlott átméretezni, hogy a későbbiekben akár egy kisebb SD-kártyán is használni tudjuk, nem csak azon, amiről a képet készítettük. A partíciók átméretezéséhez legtöbbször a GParted grafikus partíciókezelő programot szokás használni, viszont a GPartedet fizikai lemezek kezelésére találták ki, nem pedig lemezképekére. Azért, hogy mégis át tudjuk méretezni a lezeképünket GParteddel, fel kell csatolnunk úgy, mintha egy fizika meghajtó lenne.

A képünk felcsatolását a `loop` kernelmodul segítségével tehetjük meg. Valószínüleg még nincsen betöltve ez a kernelmodul, ezért a `sudo modprobe loop` parancs segítségével töltsük be, és a `sudo losetup -f` paranccsal kérjünk egy új visszacsatoló eszközt. Ha mindet jól csináltunk, valami ilyesmit kell visszakapnunk:
```bash
/dev/loop0
```
Most a `sudo losetup /dev/loop0 lemekép.img` parancs kiadásával fel kell csatolnunk a lemezképünket a `/dev/loop0` eszközbe, valamint a `sudo partprobe /dev/loop0` paranccsal be kell töltenünk a `/dev/loop0` eszközön található összes partíciót.

Ezek után már nincs más dolgunk, mint a `sudo gparted /dev/loop0` parancs segítségvel elindítani a GPartedet és az utolsó partícióra klikkelve az átméretezés menüpontban lecsökkenteni a méretét a minimumra. Ha lecsökkentettük a partíciónk méretét, klikkeljünk az alkalmazás gombra, és el is kezdődik az átméretezés. Ez a művelet a partíció méretétől, valamint az adatok mennyiségétől függően 10-15 percig is eltarthat.

Amennyiben befejeződött az átméretezés, akkor már nincs szükségünk a visszacsatoló eszközre, ezért a `sudo losetup -d /dev/loop0` paranccsal távolítsuk el.

Bár a partíciónk méretét lecsökkentettük, a lemezképünk mérete változatlan maradt. Ez azért van, mert még le kell vágnunk a partíció csökkentéssel felszabadított üres részt.

A vágáshoz szükségünk lesz arra, hogy tudjuk, hol ér véget az utolsó partíciónk. Ezt az információt a `sudo fdisk -l lemezkép.img` parancs kimenetéből tudhatjuk meg:
```bash
Disk lemezkép.img: 6144 MB, 6144000000 bytes, 12000000 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000ea37d

      Device Boot      Start         End      Blocks   Id  System
lemezkép.img1          2048        9181183    4589568   b  Ext4
```

A fenti kimenetből nekünk két dolog fontos:
- A `Units` érték (A fenti példában 512)
- Az utolsó partíció `End` értéke (A fenti példában 9181183)

A fölösleges üres hely eltávolításához a `truncate` nevű programot fogjuk használni. A méretcsökkentést a kövekező paranccsal tudjuk véghezvinni:
```bash
sudo truncate --size=$[(End+1)*Units] lemezkép.img
```
Most már rendelkezünk egy kis méretű visszaállítható lemezképpel, amit még a könnyebb tárolhatóság érdekében a `sudo xz lemezkép.img` parancs használatával még kisebbé tehetünk.