---
title: "Virtuális gépek létrehozása Virt-Managerrel"
date: 2020-06-18T09:49:33+02:00
draft: false
tags: [ "Hogyan használd" ]
image: /images//Virt-Manager-Banner.png
---
Biztosan mindenki használt már valamilyen, virtuális gépek létrehozására szolgáló programot, ha még nem, akkor is biztosan hallott már róluk.
A legtöbb ilyen program egy futtatási környezetet hoz létre a gazda operációs rendszeren.
Ezáltal a kpróbálni kíván operációs rendszer teljesítménye sokkal lassabb lassab lesz mint hogyha azt egy éles környezetben próbálnámk ki.
Viszont ma már léteznek olyan programok is, amelyek a gazda-operációsrendszer magjában hajtják végre a virtualizációt, drasztikus teljesítménycsökkenés okozása nélkül.
Az egyik ilyen program, a Virt-Manager, amely a Linux-rendszerek beépített virtualizációs modulját, a [KVM](https://hu.wikipedia.org/wiki/Kernel-based_Virtual_Machine)-et használja.
<!--more--> 
Mielőtt bármit is telepítenénk, le kell ellenőriznünk, hogy a gépünk kompatibilis-e a KVM-mel. Ezt úgy tehetjük, meg hogy a terminálunkban kiadjuk a következő parancsot:

```bash
LC_ALL=C lscpu | grep Virtualization
```

Ha eredményként azt kapjuk, hogy `AMD-V` vagy pedig `VT-x` akkor minden rendben van, mert a processzorunk támogatja a KVM-et.

Ahhoz, hogy tudjunk kommunikálni grafikus felületen a KVM-mel, fel kell telepítenünk még néhány programot. A programok telepítését a következő parancsok egyikével tudjuk megtenni:
- Debian/Ubuntu-alapú rendszerek esetén: `sudo apt install qemu virt-manager edk2-ovmf ebtables`
- Arch-alapú rendszerek esetén: `sudo pacman -S qemu virt-manager edk2-ovmf ebtables`

A programok telepítése után még hozzá kell adnunk a felhasználónkat a libvirt nevű csoporthoz, hogy kezelni tudjuk a virtuális gépeinket. A csoporthoz az alábbi parancsokkal tudjuk a felhasználónkat hozzáadni:
- Debian/Ubuntu-alapú rendszerek esetén: `sudo adduser felhasználónév libvirt`
- Arch-alapú rendszerek esetén: `sudo usermod -G libvirt -a felhasználónév`

Ezek után már nincs más dolgunk, mint a `sudo systemctl start libvirtd && sudo systemctl enable libvirtd` parancs kiadásával elindítani a Virt-Manager démonját, és már használhatjuk is.

A Virt-Manager a .qcow2 formátumot használja a Virtualbox .vdi formátuma helyett.
De a már meglévő virtualboxos gépeinket a `sudo qemu-img convert -f vdi -O qcow2 gép.vdi /var/lib/libvirt/images/gép.qcow2` parancs kiadásával egyszerűen át tudjuk alakítani a Virt-Managernek megfelelő formátumba.
