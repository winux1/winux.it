---
title: "Hangkimenet automatikus átállítása HDMI kábel csatlakoztatásakor"
date: 2020-06-18T09:48:06+02:00
draft: false
tags: [ "Egypercesek" ]
image: images/HDMI-Banner.png
---
Biztosan ti is jártatok már úgy, hogy amikor a laptopotokat rákötöttétek a TV-re, hogy megnézzétek a kedvenc sorozatotokat Netflixen, elfelejtettétek átállítani a hangkimenetet HDMI-re, és emiatt az egész film alatt bosszankodtatok.
Mivel sok olyan embert ismerek, akivel az előbbi eset már többször megtörtént, úgy döntöttem, hogy utánajárok annak, hogyan lehet a hangkimenet váltást automatizálni.
<!--more--> 
Először is szükségünk lesz egy scriptre, ami a megfelelő pillanatban lefut és elvégzi a váltást. Én a következő scriptet szoktam használni:

```bash
!/bin/bash
export PATH=/usr/bin
USER_NAME=$(who | awk -v vt=tty$(fgconsole) '$0 ~ vt {print $1}')
USER_ID=$(id -u "$USER_NAME")
CARD_PATH="/sys/class/drm/card0/"
AUDIO_OUTPUT="analog-surround-40"
PULSE_SERVER="unix:/run/user/"$USER_ID"/pulse/native"
for OUTPUT in $(cd "$CARD_PATH" && echo card*); do
OUT_STATUS=$(<"$CARD_PATH"/"$OUTPUT"/status)
if [[ $OUT_STATUS == connected ]]
then
echo $OUTPUT connected
case "$OUTPUT" in
"card0-HDMI-A-1")
AUDIO_OUTPUT="hdmi-stereo" # Digital Stereo (HDMI 1)
;;
"card0-HDMI-A-2")
AUDIO_OUTPUT="hdmi-stereo-extra1" # Digital Stereo (HDMI 2)
;;
esac
fi
done
echo selecting output $AUDIO_OUTPUT
sudo -u "$USER_NAME" pactl --server "$PULSE_SERVER" set-card-profile 0 output:$AUDIO_OUTPUT+input:analog-stereo
```

Ahhoz, hogy a scriptünk magától lefusson létre kell hoznunk egy [UDEV](https://en.wikipedia.org/wiki/Udev) szabályt. A `/etc/udev/rules.d` könyvtáron belül hozzunk létre egy fájlt `99-hdmi_sound.rules` néven, a fájl tartalma pedig legyen a következő:

```bash
KERNEL=="card0", SUBSYSTEM=="drm", ACTION=="change", RUN+="/usr/local/bin/script-neve.sh
```

A szabályunk érvénybeléptetéséhez adjuk ki a következő parancsot: `sudo udevadm control --reload-rules`
Remélhetőleg a kedvenc sorozatunk következő epizódjának nézése közben már nem kell bosszankodnunk a rosszabb hangminőség miatt.
