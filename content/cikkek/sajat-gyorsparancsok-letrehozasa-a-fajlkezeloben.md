---
title: "Saját gyorsparancsok létrehozása a fájlkezelőben"
date: 2020-06-18T09:49:01+02:00
draft: false
tags: [ "Egypercesek" ]
image: /images/Gyorsparancsok-Banner.png
---
A fájlkezelő egy olyan program, amit mindenki nap mint nap használ, és legtöbbször ugyanazokat a monoton műveleteket végzi el benne, és ha az ember sokat dolgozik, ez a monotonitás egy idő után nagyon unalmassá tud válni.
Mivel a Linuxok olyanok, mint a LEGO, ezért fájlkezelőben létre tudunk hozni saját funkciókat, amivel meg tudjuk könnyíteni magunknak a mindennapi teendőket.
<!--more--> 
A gyorsparancsok elkészítéséhez először navigáljunk el a fájlkezelőnk szerkesztés menüpontjába, és a legördülő menüben válasszuk az Egyéni műveletek beállításá-t. A megnyíló ablakban tudjuk megadni a saját parancsunk tulajdonságait.
Például, ha mindig sok .docx kiterjesztésű dokumentumot kapunk, de mi .odt formátumban szeretnénk velük tovább dolgozni, akkor a következő módon létre tudjuk hozni a fájlok átkonvertálását megkönnyítő gyorsparancsunkat:

A névhez például adjuk meg azt, hogy konvertálás ODT-be, a parancs amit a program végre fog hajtani, a következő lesz: `unoconv -f odt %F`. Ezután navigáljunk át a Megjelenítési feltételek fülre.
Ezen a fülön tudjuk meghatározni, hogy milyen fájltípusok esetén jelenjen meg a környezeti menüben a parancsunk. Jelen esetben fájlmintánk a következőt: `*.doc;*.docx,` és tegyünk egy pipát az Egyéb fájlokhoz.
Ha mindent jól csináltunk, akkor egy Word-dokumentumra kattintva meg kell jelennie az általunk létrehozott menüpontnak is.
