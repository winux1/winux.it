---
title: "Univerzális nyomtatószerver Ubuntun"
date: 2021-09-26T10:00:31+02:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: images/Printserver-Banner.png
---
A nyomtatók menedzselése sok problémát tud okozni akkor hogyha egyszerre több eszközről szeretnénk őket, mert sokszor időigényes lehet a nyomtatandó fájlok eszközök közötti átmozgatása. Valamint az is gond lehet, hogy a legtöbb nyomtató nem kompatibilis minden eszközünkkel. Erre a probléma nyújthat megoldást egy Ubuntu alapú, [CUPS](https://en.wikipedia.org/wiki/CUPS) nyomtatószerver létrehozása, mely majdnem az összes nyomtatót képes egyaránt kompatibilissé tenni minden Linux, Windows illetve Apple eszközzel.
<!--more-->

Első lépésként telepítenünk kell minden a szerverünk működéséhez szükséges programot: Ezt az következő parancs kiadásával tudjuk megtenni: `sudo apt install cups samba avahi-discover` Ha programok települtek, a `sudo usermod -aG lpadmin felhasználónév && cupsctl --remote-any --share-printers` parancsal hozzá kell adnunk a felhasználónkat `lpadmin` csoporthoz és bekapcsoljuk a szerver távoli elérését, hogy felhatalmazzuk magunkat a nyomtatók kezelésére. Ezután még mindig a terminálban maradva, nyissuk meg szerkesztéshez a `/etc/samba/smb.conf` fájlt, és a végéhez adjuk hozzá a következőket:
```bash
# CUPS printing. See also the cupsaddsmb(8) manpage in the
# cupsys-client package.
printing = cups
printcap name = cups
[printers]
comment = All Printers
browseable = no
path = /var/spool/samba
printable = yes
guest ok = yes
read only = yes
create mask = 0700
# Windows clients look for this share name as a source of downloadable
# printer drivers
[print$] comment = Printer Drivers
path = /usr/share/cups/drivers
browseable = yes
read only = yes
guest ok = no
```
Ha megvagyunk, csatlakoztassuk a nyomtatónkat és indítsuk újra a szervert. A szerver újraindulását követően, egy webböngészőben látogatásunk el a https://szerver:631 címre, és az Administration fülön klikklejünk az Add Printer gombra. Ekkor az oldal meg fog kérni, hogy jelentkezzünk be. A felhasználónevünk és a jelszavunk meg fog egyezni azzal amit a terminálba történő belépéshez használunk.

{{% notice note "Megjegyzés"%}}
Amennyiben HP nyomtatóval rendelkezünk, mielőtt továbblépnénk, a nyomtatónk megfelelő működése érdekében a `sudo apt install hplip` és a `hp-plugin -i` parancsokkal telepítenünk kell a HP illesztőprogramot.
{{% /notice %}}

A megjelenő listában válásszuk ki a nyomtatónkat, majd klikkesünk a Continue gombra. Ezután nincs más dolgunk mint egy pipát rakni a Sharing opcióhoz, és ismételten a Continue gombra nyomni. A megjelenő képernyőn ki kell választanunk a nyomtatónk illesztőprogramját (ezt a CUPS a legtöbb esetben automatikusan felajánlja) a megjelenő listából, majd a végén az Add printer majd a Set Default Options gombra kattintani.

Ha mindent jól csináltunk, innentől kezdve minden eszközünk képes lesz automatikusan felfedezni a nyomtatónkat, és már kezdhetünk is nyomtatni!