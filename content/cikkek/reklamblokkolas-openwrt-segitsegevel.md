---
title: "Reklámblokkolás OpenWRT segítségével"
date: 2020-07-20T17:46:46+02:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: images/Adblock-Banner.png
---
Lassan már majdnem minden oldalon találkozhatunk reklámokkal. Sokan nem szeretik a reklámokat, mert vannak olyan esetek, amikor átláthatatlanná teszik az oldalon található szöveget.
Többen éppen ezért használnak valamilyen reklámblokkoló böngésző kiegészítőt, viszont ezekkel az a probléma, hogy telefonon nem használhatóak.
Amennyiben [OpenWRT](/cikkek/openwrt-hasznalata) fut a routerünkön, lehetőségünk van hálózati szinten blokkolni a reklámokat.
<!--more--> 
Először is az `ssh root@openwrt.lan` paranccsal lépjünk be a routerünkbe, és az `opkg update` parancs segítségével frissítsük a csomagtárolót, hogy a legfrissebb csomagok álljanak a rendelkezésünkre.
Ha ezzel megvagyunk, telepítsük a reklámblokkoló programot és a függőségeit:
```bash
opkg install adblock libustream-mbedtls luci-app-adblock luci-i18n-adblock-hu
```
Most, hogy telepítettük a szükséges programokat, először is a `uci set adblock.global.adb_enabled="1"` parancs kiadásával engedélyezzük magát a reklámblokkoló szolgáltatást.
Ahhoz, hogy a reklámblokkolónk megfelelően tudjon működni, a `uci set adblock.global.adb_backupdir="/etc/adblock"` parancs segítségével meg kell adnunk egy helyet, ahova a program biztonsági mentést tud készíteni.
Ezek után a beállításainkat a `uci commit adblock`, illetve a `/etc/init.d/adblock restart` parancsokkal tudjuk érvénybe léptetni.

Amennyiben mindent jól csináltunk, pár óra múlva  már nem fogunk találkozni egyetlen reklámmal sem.
