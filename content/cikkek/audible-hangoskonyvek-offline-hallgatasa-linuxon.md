---
title: "Audible hangoskönyvek offline hallgatása Linuxon"
date: 2021-06-27T11:26:36+02:00
draft: false
tags: [ "Egypercesek" ]
image: images/Audible-Tux-Banner.png
---
Habár az [Audible](https://www.audible.com/) manapság az egyik legnépszerűbb és talán legjobb hangoskönyveket kínáló oldal, mégsem jeleskedik a Linux rendszerek támogatásában. Bár vitathatatlanul  sokat fejlődött a megvásárolt hangoskönyvek internetkapcsolat nélküli hallgatása hivatalosan még mindig nem támogatott. Mivel az Audible megengedi a megvásárolt könyvek letöltését a böngészőnkből, csak el kell távolítanunk a [DRM](https://hu.wikipedia.org/wiki/Digit%C3%A1lis_jogok_kezel%C3%A9se)-et a letöltött fájlról és már hallgathatjuk is internetkapcsolat nélkül.
<!--more-->

{{% notice warning "Figyelem!"%}}
A cikkben leírtakat kizárólag magáncélra használd fel! A DRM-mentes hangoskönyvek engedély nélküli terjesztését **A TÖRVÉNY BÜNTETI**!
{{% /notice %}}

Első lépésként töltsük le az Audible oldaláról azt a könyvet amelyiket offline szeretnénk hallgatni. Miután letöltöttük a fájlt meg kell tudnunk a fájl checksumját mert annak a segítségével fogjuk tudni megkapni a könyvünk más formátumba történő konvertálásához szükséges kulcsot. A hangoskönyv checksumját az `ffmpeg` programcsoportba tatozó, `ffprobe`-al fogjuk megtudni.
Ha még nincs az alábbi parancsok valamelyikével telepítsük fel az `ffmpeg` programcsomagot:
- Debian/Ubuntu-alapú rendszerek esetén:`sudo apt install ffmpeg`
- Arch-alapú rendszerek esetén:`sudo pacman -S ffmpeg`

A program telepítése után nyissunk egy terminált abban a könyvtárban ahol a letöltött fájlunk van és futtassuk az `ffprobe fájlnév.axx` parancsot, majd a parancs kimenetében keressük meg az alábbi sort:
```bash
[mov,mp4,m4a,3gp,3g2,mj2 @ 0x55d45524ddc0] [aax] file checksum ==
```

Ahhoz, hogy a checksumból meg tudjuk kapni az aktiváló kulcsot le kell töltenünk egy Audible aktivációs-kód mintákat tartalmazó git tárólót:
```bash
git clone https://github.com/inAudible-NG/tables.git
```
Ha letöltöttük a tárolót nincs más dolgunk mint belépni a letöltött _tables_ mappába és kiadni az alábbi parancsot az aktivációs kód kinyeréséhez:
```bash
./rcrack . -h azffprobealkinyerthash
```

Ha lefutott a parancs, (a gépünk teljesítményétől függően több percig is eltarthat) a kimenetben keressük meg az aktivációs kódunkat amit a a _hex:_ szöveg után fogunk megtalálni. Ha megvan, lépjünk át abba a mappába ahol a letöltött hangoskönyv van és a konveráláshoz futtasuk le a következő parancsot:
```bash
ffmpeg -activation_bytes afentkinyertkód -i "hangoskönyv.aax" -codec copy "hangoskönyv.m4b"
```

{{% notice tip "Megjegyzés"%}}
Az aktivációs kódot elég egyszer kinyerni, mivel minden egyes ugyanazzal a fiókkal megvásárolt könyvnél megegyezik
{{% /notice %}}

Amennyiben mindent jól csináltunk, a fenti parancs eredményeként kapott m4b kiterjesztésű fájlt bármilyen médialejátszó segítségével meghallgathatjuk.
