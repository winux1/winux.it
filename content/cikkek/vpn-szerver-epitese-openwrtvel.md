---
title: "VPN-szerver építése OpenWRT-vel"
date: 2020-08-13T16:20:48+02:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: /images//VPN-Banner.png
---
Ha szeretnénk gyorsan és biztonságosan elérni az otthoni hálózatunkat, célszerű beüzemelnünk a saját [VPN](https://hu.wikipedia.org/wiki/Virtuális_magánhlózat) szerverünket. A legegyszerűbben ezt az [OpenVPN](https://openvpn.net/), illetve egy [OpenWRT](/cikkek/openwrt-hasznalata)-t futtató router segítségével tehetjük meg.
<!--more--> 
{{% notice info Fontos%}}
Amennyiben korábban telepítettük a `luci-ssl` csomagot a routerünkre, a `opkg remove luci-ssl && /etc/init.d/uhttpd restart` parancs segítségével távolítsuk el ezt, mert ütközni fog a VPN-tanúsítványaival!
{{% /notice %}}

Először is az `opkg update` paranccsal frissítsük a tárolókat, majd az `opkg install openvpn-easy-rsa luci-app-openvpn openvpn-openssl nano` parancs kiadásával telepítsük a szükséges csomagokat! Ha megvagyunk, le kell generálnunk még pár tanúsítványt a VPN-szerverünk számára. Az `mkdir /etc/config/openvpn-config` paranccsal elkészítjük a VPN-kiszolgálónk konfigurációs mappáját. Ebbe a mappába az `mv /etc/easy-rsa/* /etc/config/openvpn-config/` parancs segítségével átmozgatjuk a /etc/easy-rsa mappából a fájlokat, és az `rm -rf /etc/easy-rsa/` paranccsal töröljük azt. Ezután pedig az `ln -s /etc/config/openvpn-config/ /etc/easy-rsa` paranccsal létrehozunk egy, az új mappáról a régire mutató symlinket.

Ezek után nanoval szerkesztenünk kell a `/etc/easy-rsa/vars` fájlt. Ebben a fájlban keressük ki az az alábbi sorokat, vegyük ki előlük a `#`-et, valamint értelemszerűen módosítsuk őket:

```
set_var EASYRSA_REQ_COUNTRY ""
set_var EASYRSA_REQ_PROVINCE ""
set_var EASYRSA_REQ_CITY ""
set_var EASYRSA_REQ_ORG ""
set_var EASYRSA_REQ_EMAIL ""
set_var EASYRSA_REQ_OU ""
```
A `set_var EASYRSA_KEY_SIZE=2048` sor elől is távolítsuk el a `#`-et, de ennek az értékén ne változtassunk. A következő lépésben lépjünk be a `/etc/easy-rsa/` könyvtárba, és az `init-pki` paranccsal léptessük érvénybe az előzőleg véghez vitt módosításainkat. Majd az `easyrsa build-ca` paranccsal elkészítjük a tanúsítványkibocsájtó tanúsítványunkat. Mivel ezt a tanúsítványt érdemes jól megvédeni, fontos, hogy erős jelszót adjunk meg. Miután elkészült a tanúsítványunk, szükséges még létrehoznunk egy [Diffie-Hellman](http://snailman.web.elte.hu/pub/diffie-hellman/diffie-hellman/dh-f2.htm)-kulcsot is. A kulcs létrehozásához egyszerűen csak adjuk ki az `easyrsa gen-dh` parancsot. A Diffie-Hellman-kulcs elkészítése, a routerünk teljesítményétől függően körülbelül 10-15 percet is igénybe vehet, úgyhogy legyünk türelmesek. Miután elkészült a Diffie-Hellman-kulcsunk, az `easyrsa gen-req server nopass && easyrsa sign-req server server` hozzuk létre a szerverünk tanúsítványát és kulcsát.

A következőkben az alábbi parancsokkal létrehozzuk azt az interfészt, amin keresztül a VPN-szerver kommunikálni fog:

```bash
uci set network.vpn0="interface"
uci set network.vpn0.ifname="tun0"
uci set network.vpn0.proto="none"
uci set network.vpn0.auto="1"
uci commit network
```
A következő parancsokkal pedig beállítjuk a tűzfalat, hogy engedélyezze a VPN-kommunikációt:

```bash
uci add firewall rule
uci set firewall.@rule[-1].name="Allow-OpenVPN-Inbound"
uci set firewall.@rule[-1].target="ACCEPT"
uci set firewall.@rule[-1].src="wan"
uci set firewall.@rule[-1].proto="udp"
uci set firewall.@rule[-1].dest_port="1194"
uci add firewall zone
uci set firewall.@zone[-1].name="vpn"
uci set firewall.@zone[-1].input="ACCEPT"
uci set firewall.@zone[-1].forward="ACCEPT"
uci set firewall.@zone[-1].output="ACCEPT"
uci set firewall.@zone[-1].masq="1"
uci set firewall.@zone[-1].network="vpn0"
uci add firewall forwarding
uci set firewall.@forwarding[-1].src="vpn"
uci set firewall.@forwarding[-1].dest="wan"
uci add firewall forwarding
uci set firewall.@forwarding[-1].src="vpn"
uci set firewall.@forwarding[-1].dest="lan"
uci commit firewall
```
Majd az`/etc/init.d/network reload` és az `/etc/init.d/firewall reload` parancsokkal érvénybe léptetjük a változtatásainkat.

Ezek után az alább található parancsokkal engedélyezzük a VPN-ünket (A VPN-ünk neve _myvpn_ lesz), beállítjuk a VPN-protokollt és a kommunikációs portot:

```bash
uci set openvpn.myvpn="openvpn"
uci set openvpn.myvpn.enabled="1"
uci set openvpn.myvpn.dev="tun"
uci set openvpn.myvpn.port="1194"
```
A következő parancsokkal, pedig megadjuk azt, hogy mennyi ideig tartsa életben a kapcsolatot a szerver, amennyiben nem kap jelet a klienstől, valamint beállítjuk a logfájlok elérési útját:

```bash
uci set openvpn.myvpn.comp_lzo="yes"
uci set openvpn.myvpn.status="/var/log/openvpn_status.log"
uci set openvpn.myvpn.log="/tmp/openvpn.log"
uci set openvpn.myvpn.verb="3"
uci set openvpn.myvpn.mute="5"
uci set openvpn.myvpn.keepalive="10 120"
uci set openvpn.myvpn.persist_key="1"
uci set openvpn.myvpn.persist_tun="1"
```
Azért, hogy minimalizáljuk a biztonsági kockázatokat, nem szabad rootként futtatni a VPN-ünket, ezért az alábbi két paranccsal megváltoztatjuk a szervert futtató felhasználót:

```bash
uci set openvpn.myvpn.user="nobody"
uci set openvpn.myvpn.group="nogroup"
```
A következőkben megadjuk, az előzőleg elkészített tanúsítványok elérési útját az OpenVPN-nek:

```bash
uci set openvpn.myvpn.ca="/etc/config/openvpn-config/pki/ca.crt"
uci set openvpn.myvpn.cert="/etc/config/openvpn-config/pki/issued/server.crt"
uci set openvpn.myvpn.key="/etc/config/openvpn-config/pki/private/server.key"
uci set openvpn.myvpn.dh="/etc/config/openvpn-config/pki/dh.pem"
```
Most ténylegesen megmondjuk a routerünkön futó OpenVPN-nek, hogy szerverként működjön, és beállítjuk, hogy a csatlakozott kliensek lássák egymást a hálózaton:

```bash
uci set openvpn.myvpn.mode="server"
uci set openvpn.myvpn.tls_server="1"
uci set openvpn.myvpn.server="10.8.0.0 255.255.255.0"
uci set openvpn.myvpn.topology="subnet"
uci set openvpn.myvpn.route_gateway="dhcp"
uci set openvpn.myvpn.client_to_client="1"
```

Most már majdnem megvagyunk a VPN konfigurációjával. Már csak az van hátra, hogy megadjuk a szerverünknek, hogy milyen beállításokat továbbítson a klienseknek csatlakozáskor:

```bash
uci add_list openvpn.myvpn.push="comp-lzo yes"
uci add_list openvpn.myvpn.push="persist-key"
uci add_list openvpn.myvpn.push="persist-tun"
uci add_list openvpn.myvpn.push="user nobody"
uci add_list openvpn.myvpn.push="user nogroup"
uci add_list openvpn.myvpn.push="topology subnet"
uci add_list openvpn.myvpn.push="route-gateway dhcp"
uci add_list openvpn.myvpn.push="redirect-gateway def1"
uci add_list openvpn.myvpn.push="route 10.10.1.0 255.255.255.0"
uci commit openvpn
```

Ezek után nincsen már más dolgunk, mint a `/etc/init.d/openvpn start` parancsal elindítani, majd a `/etc/init.d/openvpn enable` engedélyezni a VPN-szolgáltatást.

Ahhoz, hogy a későbbiekben csatlakozni tudjunk a VPN-ünkhöz, meg kell nyitnunk a 1194-es portot az internet felé. Ezt legkönnyebben grafikusan, a http://openwrt.lan címen tudjuk megtenni a Hálózat &rarr; Tűzfal &rarr; Port Forwards menüpontba navigálva. Itt hozzunk létre egy új továbbítási szabályt: A `Protocoll`-t, állítsuk `udp`-re, a `Source zone` legyen `wan`, az `Extenal`, illetve az `Internal port`-ot állítsuk egyaránt `1194`-re, a `Source zone`-nak pedig adjuk meg a `vpn0` zónát.
![vpn-port-forward.png](/images/vpn-port-forward.png)

Most már a szerverünk készen áll a kliensek fogadására, itt az idő, hogy létrehozzuk a klienskulcsainkat és a kliensek _.ovpn_ fájlját. Klienskulcsokat az `easyrsa build-client-full felhasználónév nopass` paranccsal tudjuk létrehozni.

Az alább található openvpn fájl megfelelelő helyeire másoljuk be a kulcsainkat, majd mentsük el _.ovpn_ kiterjesztéssel, és ha mindent jól csináltunk a `sudo openvpn felhaználónév.ovpn` parancs kiadásával, csatlakozni is tudunk a saját VPN-ünkhez.

```
dev tun
proto udp

<ca>
-----BEGIN CERTIFICATE-----
ca.crt (/etc/easy-rsa/pki/ca.crt)
-----END CERTIFICATE-----

</ca>

<cert>
-----BEGIN CERTIFICATE-----
felhaszálónév.crt (/etc/easy-rsa/pki/issued/felhaszálónév.crt)
-----END CERTIFICATE-----

</cert>

<key>
-----BEGIN PRIVATE KEY-----
felhaszálónév.key (/etc/easy-rsa/pki/private/felhaszálónév.key)
-----END PRIVATE KEY-----

</key>

client
float
remote-cert-tls server
remote example.hu 1194
````
