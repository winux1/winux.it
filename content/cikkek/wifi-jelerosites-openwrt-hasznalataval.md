---
title: "Wi-Fi-jelerősítés OpenWRT használatával"
date: 2020-08-04T12:08:11+02:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: images/Openwrt-Banner.png
---
Ha nagy házban lakunk, biztosan sokat szenvedtünk már azzal, hogy a házunk minden részében elfogadható erősségű legyen a vezeték nélküli internet. Amennyiben nem tudjuk a házunkban mindenhova elvezetni az internetkábelt, célszerű beüzemelnünk egy Wi-Fi-jelerősítőt. Jelerősítőként használhatunk egy erősebb routert is, de természetesen a legjobb hatást valamilyen [OpenWRT](/cikkek/openwrt-hasznalata) kompatibilis Wi-Fi-erősítő használatával érhetjük el
(ilyen eszköz például a [TP-Link RE450](https://www.tp-link.com/hu/home-networking/range-extender/re450/)).
<!--more-->
A két eszköz a WDS (**W**ireless **D**istribution **S**ystem) segítségével kapcsolódik egymáshoz. A WDS-kapcsolatot tehát arra találták ki, hogy egy Wi-Fi-s eszköz közbeiktatásával távolabbra elnyúljon a hálózatunk. A legfontosabb különbség a többi repeaterkapcsolathoz képest, hogy itt a fő router tisztában van azzal, hogy mögötte egy másik eszköz is szórja a Wi-Fi-t, ezzel kibővítve a lefedett területet, és ezen a teljes területen menedzseli a kapcsolódó klienseket aszerint, hogy melyik kliens melyik Wi-Fi hozzáférési ponthoz van közel, és melyik eszköz milyen forgalmat bonyolít. Tehát a WDS igencsak okos módon tudja variálni a teljes Wi-Fi-hálózatot, ráadásul mindezt úgy, hogy ebből a klienseket használók nem sokat vesznek észre: nincsen se szakadozás, se SSID-váltás.

Első lépésként lépjünk be a fő routerünkbe, és a Hálózat &rarr; Vezeték nélküli menüpontban változtassuk meg a módot `Hozzáférési  pont (WDS)`-re
![wds-hozzaferesi-pont.png](/images/wds-hozzaferesi-pont.png)
Ez után, kábelen kössük össze a számítógépünket a jelerősítőnkkel (A Wi-Fi-t tiltsuk le!), és a Hálózat &rarr; Csatolók menüpontban távolítsuk el az összes csatolót a LAN-interfész kivételével. Majd a LAN-interfész beállításainál állítsunk be a fő routerünktől eltérő IP-címet a repeaterünknek. Az átjárót és a DNS-t állítsuk be a fő routerünk IP-címére.
![repeater-lan.png](/images/repeater-lan.png)
Majd navigáljunk át a LAN-csatoló DHCP-fülére, ahol pipáljuk be a `DHCP letiltása ennél a csatolónál` opciót, valamint a DHCP-beállítások IPv6-fülén állítsunk mindent `letiltott` állapotúra.
![repeater-lan-dhcpv4.png](/images/repeater-lan-dhcpv4.png)
![repeater-lan-dhcpv6.png](/images/repeater-lan-dhcpv6.png)

A következőkben ténylegesen össze fogjuk kapcsolni a jelerősítőnket a routerünkkel. Ehhez menjünk a jelerősítőnk Hálózat &rarr; Vezeték nélküli menüpontjába, ahol klikkeljünk a `Keresés` gombra. A megjelenő ablakban adjuk meg a fő routerünk, Wi-Fi-jének jelszavát, majd pipáljuk be a `Vezeték nélküli beállítások cseréje` és a `Zárolás BSSID-hoz` opciókat, valamint a `Tűzfalzóna hozzárendelése vagy létrehozása` legördülő menüben válasszuk a LAN-zónát.
![repeater-kapcsolodas.png](/images/repeater-kapcsolodas.png)
Ezután a megjelenő párbeszédablakban változtassuk meg a módot `Ügyfél (WDS)`-re, valamint a hálózatot állítsuk át LAN-ra.
![wds-ugyfel.png](/images/wds-ugyfel.png)

Az előző lépések után már nincs más dolgunk, mint várni, addig amíg a gépünkön bejönnek a weboldalak. Ez a hálózatunk nagyságától, és a csatlakozott kliensek mennyiségétől függően 15-30 percig is eltarthat. Fontos, hogy türelmesek legyünk, mert amennyiben túl korán végezzük el a következő lépéseket, az a teljes hálózatunk összeomlásához vezethet!

Ha már bejönnek az oldalak a gépünkön, navigáljunk a Hálózat &rarr; DHCP és DNS menüpontba, ahol a `DNS-továbbításoknál` adjuk meg a fő routerünk IP-címét.
![repeater-dhcp.png](/images/repeater-dhcp.png)

Visszatérve a Hálózat &rarr; Vezeték nélküli menüpontba, a hozzáadás gombbal adjunk hozzá egy új Wi-Fi-interfészt, amin állítsuk be ugyanazokat a beállításokat, mint a fő routerünkön, azért, hogy Wi-Fi-vel is tudjunk kapcsolódni a repeaterünkhöz.

Azért, hogy megakadályozzuk a Wi-Fi-hálózatunk összeomlását, célszerű bekapcsolni az [STP](https://en.wikipedia.org/wiki/Spanning_Tree_Protocol/) (**S**panning **T**ree **P**rotocol) protokollt. Ezt úgy tudjuk megtenni, hogy a Hálózat &rarr; Vezeték nélküli menüpontban a LAN-interfész  fizikai beállításainál bepipáljuk az `STP engedélyezése` opciót. Miután ezzel megvagyunk, a csatolók közül nyugodtan törölhetjük a WWAN-interfészt.
![repeater-lan-stp.png](/images/repeater-lan-stp.png)

Ha mindent jól csináltunk, akkor remélhetőleg már a házunk egész területén elérhető lesz a Wi-Fi-hálózat.