---
title: "Keresés nyomkövetés nélkül, avagy a Searx metakereső használata"
date: 2021-02-06T14:51:54+01:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: images/Searx-Banner.png
---

Amikor az interneten keresünk, a legtöbb keresőmotor naplózza a kereséseinket, hogy a késibbekben a keresési statisztikáink alapján, célzott reklámokkal támadjon minket. Sokan nem szeretik ezt, ezért keresnek maguknak valamilyen privát-szféra tisztelő keresőmotort (Ilyen például a [DuckDuckGo](https://duckduckgo.com/) vagy éppen a [Swisscows](https://swisscows.com/).) Ezekkel a kisebb, de barátságosabb keresőmotorokkal az a probléma, hogy szerverkapacitás hiányában képtelenek felvenni a harcot a nagy ellenfelekkel szemben. Erre a problémára kínál megoldást a [Searx](https://searx.me) [metakereső](https://hu.wikipedia.org/wiki/Metakeres%C5%91). Azaz A Searx nem végez indexelést, hanem a tradicionális keresőmotorok eredményeit rendszerezi és fűzi össze egy találati listába. Ez azért biztonságosabb mert nem mi kommunikálunk nagy keresőkkel, hanem a Searx szerver, így nem tudnak lekövetni minket.
<!--more-->
Ha kedvet kaptunk, és ki szeretnénk próbálni a Searx-t nincs más dolgunk mint felmenni a https://searx.space oldalra ahol választhatunk egyet a publikus példányok közül és már kezdhetünk is böngészni. Amennyiben tetszik a Searx, célszerű egy saját példányt használni, mert így biztosak lehetünk benne, hogy az adataink jó kezekben vannak

A saját példányunk létrehozásához először klónozzuk le a Searx git tárolóját a `git clone https://github.com/searx/searx.git` parancsot használva. Ha megvagyunk a klónozással. lépjünk be a letöltött mappába és már kezdődhet is a telepítés!

{{% notice info Fontos%}}
A telepítő scriptek **CSAK** Debian/Ubuntu rendszerek alatt működnek!
{{% /notice %}}

Legelső lépésként az alábbi paranccsal telepítjük a Searx-t (minden kérdésre válaszoljunk _yes_-el):
```bash
sudo -H ./utils/searx.sh install all
```

Ha a Searx sikeresn települt szükségünk van még a Filtron és Morty proxykra, hogy teljes mértékben megvédjenek minket a nagy kereső motorok fürkésző szemeitől. A lent látható paranccsal telepítsük őket, és minden kérdésre válaszoljunk _yes_-szel:
```bash
sudo -H ./utils/filtron.sh install all && sudo -H ./utils/morty.sh install all
```

Most már nincs més dolgunk, mint kiválasztani, hogy Nginx-et vagy Apache-ot szeretnénk-e használni a Searx-hez

- Amennyiben az Apache-ot választottuk, futtassuk a következő parancsot:
```bash
sudo -H ./utils/filtron.sh apache install && sudo -H ./utils/morty.sh apache install
```
- Ha viszont Nginx-el szeretnénk használni a Searx-et az alábbi parancs lesz a segítségünkre:
```bash
sudo -H ./utils/filtron.sh nginx install && sudo -H ./utils/morty.sh nginx install
```

Ha feltelepült a webszerverünk az _ipcím_/searx címen érhetjük el. Amennyiben nem a /sear címen szeretnénk elérni a példényunkat a kovetkezőt kell tennünk:

- Apache webszerver esetén:
Navigáljunk a /etc/apache2/sites-available könyvtárba távolítsuk el a _morty.conf_ fájlt a _searx.conf_ tartalmát írjuk felül ezzel:

```apache
LoadModule proxy_module         /usr/lib/apache2/modules/mod_proxy.so
LoadModule proxy_http_module    /usr/lib/apache2/modules/mod_proxy_http.so

<VirtualHost *:80>
Servername domainnevem.valami
ServerAlias www.domainnevem.valami
ProxyPreserveHost On
ProxyPass /morty/ http://127.0.0.1:3000/
ProxyPassReverse /morty/ http://127.0.0.1:3000/
ProxyPass / http://127.0.0.1:4004/
CustomLog /dev/null combined
</VirtualHost>
```
A mohosításaink érvénybeléptetéséhez pedig futtassuk a `systemctl restart apache2` parancsot.

- Nginx webszerver esetén:
Navigáljunk a /etc/nginx/default.apps-available könyvtárba távolítsuk el a _morty.conf_ és _searx.conf_ fájlokat majd lépjünk át a /etc/nginx/sites-available könyvtárba, majd hozzunk létre egy fájlt a következő tartalommal:

```nginx
server {
  server_name domainnevem.valami;

  listen 80;
  listen [::]:80;

  location / {
           proxy_pass         http://127.0.0.1:4004/;

           proxy_set_header   Host             $http_host;
           proxy_set_header   Connection       $http_connection;
           proxy_set_header   X-Real-IP        $remote_addr;
           proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
           proxy_set_header   X-Scheme         $scheme;
  }
  location /morty/ {
           proxy_pass         http:/127.0.0.1/;

           proxy_set_header   Host             $http_host;
           proxy_set_header   Connection       $http_connection;
           proxy_set_header   X-Real-IP        $remote_addr;
           proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
           proxy_set_header   X-Scheme         $scheme;
  }

 error_log /dev/null;
 access_log /dev/null;
}
```
A mohosításaink érvénybeléptetéséhez pedig futtassuk a `systemctl restart nginx` parancsot.

Amennyiben mindent jól csináltunk, a http://_ipcím_/ címen fogjuk tudni elérni a keresőnket.