---
title: "Hardveres gyorsítás bekapcsolása Raspberry Pi-on, Ubuntu alatt"
date: 2020-10-24T14:33:34+02:00
draft: false
tags: [ "Egypercesek" ]
image: images/Raspberry-Pi-Hardware-Acceleration-Ubuntu-Banner.png
---

Ahogy fejlődik a tudomány, a kicsi dolgok is egyre hatékonyabbá és erősebbé válnak. Ma már biztonsággal kijelenthetjük, hogy a Raspberry Pi nyogodtan használható szerverként, vagy éppen asztali számítógépként mindennapi feladataink elvégzésére. Azért, hogy a Pi-unk minden erőforrását a végletekig kihasználjuk, célszerű bekapcsolnunk rajta Ubuntu alatt is a hardveres gyorsítást.
<!--more-->
Először is vizsgáljuk meg, hogy az ezközünkön be van-e már kapcsolva a hardveres gyorsítás. Ha `ls /dev/dri` parancsra hibaüzenetet kaptunk, az azt jelenti, hogy a rendszerünk nem tudja elérni a hardveres gyorsítót. Hogy  elérjük a hardveres gyorsítóeszköt, be kell töltetnünk, a hozzá tartózó overlay-t. Az overlay betöltéséhez nyissuk meg a `/boot/firmware/usercfg.txt` fájlt szerkesztésre, majd írjuk bele a lejjebb található sorok egyikét:

- Raspberry Pi 3 esetén:
```
gpu_mem=256
dtoverlay=vc4-kms-v3d
```
- Raspberry Pi 4 esetén pedig:
```
gpu_mem=320
dtoverlay=vc4-kms-v3d-pi4
```
Egy újraindítást követően már be kell töltődnie a hardveres gyorsítóeszköz driver-ének is.

Ellenőrzésképpen futtassuk le újból az `ls /dev/dri` parancsot. Amennyiben a kimenet megegyezik az alábbi képen láthatóval, az azt jelenti, hogy mindent jól csináltunk, és a Pi-unk képes akár a 4K-s videók lejátszására is!
![hardware-acceleration-enabled-ubuntu.png](/images/hardware-acceleration-enabled-ubuntu.png)