---
title: "Az OpenWRT használata avagy hogyan kerül Linux a routerre?"
date: 2020-07-18T10:51:18+02:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: images/Openwrt-Banner.png
---
A routerek napjainkban szinte már mindenkinek behálózzák az életét.
A routerekkel legtöbbször az a probléma, hogy minden gyártó a saját egyedi kinézetű firmware-rel szállítja az eszközeit.
Ezért, hogyha routert váltasz, szinte teljesen újra kell tanulnod a routered használatát.
Ennek a problémának a megoldására jött létre az [OpenWRT](https://openwrt.org/) projekt, amelynek célja, hogy egy egységes kinézetű kezelőfelületet biztosítson routereink számára.
<!--more--> 

Az OpenWRT telepítése egy egészen egyszerű folyamat. Először is [ezen](https://openwrt.org/toh/start) az oldalon elenőrizzük le, hogy routerünk támogatott-e.
Amennyiben igen, a routerünk oldaláról töltsük le a `squashfs-factory.bin` fájlt, majd a routerünk firmware-frissítőjével telepítsük azt.
Mivel alapértelemezett állapotban az OpenWRT-ben nincs engedélyezve a Wi-Fi, fontos, hogy kábelesen csatlakozzunk a routerünkhöz.

{{% notice info Fontos%}}
Az OpenWRT telepítése garaciavesztéssel jár!
{{% /notice %}}

Miután telepíttetük az OpenWRT-t az `ssh root@openwrt.lan` parancs kiadásával lépjünk be a routerünkbe, és a `passwd` parancs segítségével hozzunk létre egy jelszót magunknak.
Ezek után, ha a böngészőnkkel elnavigálunk a http://openwrt.lan oldalra, már meg fog jelenni az OpenWRT kezelőfelülete.
A bejentkezést követően navigáljunk a `Network` &rarr; `Interfaces` menüpontba, ahol keressük meg a `WAN` interfészt, és kattintsunk az Edit gombra.
A `Protocol` menüpont alatt válasszuk ki az általunk használt kapcsolódási protokollt, és kattintsunk a `Save` majd a `Save & Apply` gombokra. Ha mindent jól csináltunk, most már kell, hogy legyen élő internetkapcsolatunk.

Mivel az OpenWRT egy teljes értékű Linux disztribúció, így rendelkezik csomagkezelővel.
Mint minden Linuxnál, itt is frissíteni kell néha a tárolókat, illetve a csomagokat.
Ezt legkönnyebben SSH-n keresztül, az `opkg update` paranccsal tehetjük meg. Amennyiben szeretnénk, hogy a routerünk magyarul kommunikáljon velünk, az `opkg install` parancs segítségével fel kell telepítenünk a `luci-i18n-base-hu` csomagot.
