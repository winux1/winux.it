---
title: "Saját E-mail szerver létrehozása, Power Mail-in-a-Box segítségével"
date: 2022-01-15T15:55:24+01:00
draft: false
tags: [ "Szerverek és hálózatok" ]
image: images/Mail-Banner.png
---
A technikai fejlődésnek köszönhetően a papír alapú levelezést szinte teljesen felváltották az E-mailek, és habár az elektronikus levél bizonyos tekintetben sokkal nehezebben juthatnak illetéktelen kezekbe, azért nem árt megválogatni, hogy melyik E-mail szolgáltatónál nyitunk fiókot. Azt szokták mondani, hogy az a legbiztonságosabb szolgáltató, aminek a legtöbb ügyfele van, mivel hogyha nem lennének biztonságosak az ügyfeleik elpártolnának tőlük. A probléma viszont az, hogyha egy bizonyos cég az adott területen egyeduralkodóvá válik, akkor szinte bármit megtehet, az emberek mindig náluk fognak maradni. Azért, hogy függjünk a rendszert éppen uraló nagyvállalatoktól, melynél az E-mail címmel rendelkező emberek 90 százalékának van fiókja, célszerű létrehozni magunknak egy saját E-mail szervert, ami bár pénzbe kerül, 525 forintos havidíj (a domain név ára) ellenében teljes biztonságban tudhatjuk a leveleinket. Általában egy levelező szerver bonyolult és időigényes feladat szokott lenni, viszont a [Power Mail-in-a-Box](https://power-mailinabox.net) projekt fejlesztőinek sikerült egy olyan kompakt megoldással előállni, amivel szinte bárki percek alatt csinálhat magának egy levelező szervert.
<!--more-->
A levelezőszerverünk létrehozásához szükségünk lesz egy VPS-re, mert az otthoni internetszolgáltatók, a levélszemétgyártás elkerülésének érdekében blokkolják a levelek küldéséhez elengedhetetlen 25-ös porton keresztül történő kommunikáció. Ha csak pár E-mail szoktunk naponta küldeni az Oracle szolgáltatása megfelelő lehet számunkra ahol teljesen ingyen vehetünk igénybe egy VPS-t, aminek specifikációi pontosan megfelelőek egy kevés felhasználót kiszolgáló Power Mail-in-a-Box szerver futtatásához. Ahhoz hogy jogosulttá váljunk az Oracle szolgáltatására, látogassunk el a https://www.oracle.com/cloud/free/ oldalra ahol Kattintsunk a _Start for free_ feliratú gombra
![Oracle-start.png](/images/Oracle-start.png)
A megjelenő űrlapot adjuk meg a nevünket és az országunkat valamint egy E-mail címert amire a rendszer tud küldeni egy hitelesítő linket. A hitelesítő linkre kattintva meg kell még adnunk egy felhasználónév és jelszó párost, valamint ki kell választanunk, hogy melyik szerverközpont felhasználói szeretnénk lenni. Fontos hogy olyan szerverközpontot válasszunk amely közel van hozzánk mert ez nagyban meg tudja könnyíteni a későbbi ügyintézést. Ha kitöltöttük az űrlapot, még meg kell adnunk egy bánkártyát amire egy egyszeri 300 Ft-os terhelés fog történni melyet nem sokkal később vissza fognak utalni, ezáltal leellenőrizve a személyazonosságunkat. Ez után a regisztrációval végeztünk is viszont a fiókunk aktiválása egy-két napot is igénybe vehet (az aktiválásról E-mailben fognak értesíteni).
Ha megkaptuk a regisztrációnk aktiválásáról szóló E-mailt, látogassunk el a https://www.oracle.com/cloud/sign-in.html oldalra és adjuk meg a felhasználónevünket. Ezután a következő oldalon a felhasználónév mezőbe írjuk be az E-mail címünket, a jelszó rublikába pedig értelemszerűen a regisztrációkor megadott jelszót írjuk, majd lépjünk be. Az Oracle-nél alapértelmezetten nincsen nyitva a 25-ös port, ezért kérvényeznünk kell, hogy kommunikálhassunk rajta, viszont az Oracle csak akkor tudja teljesíteni a kérésünket, hogyha a profilunkat felminősíttetjük _Pay as you go_ típusúvá, mely ugyanúgy ingyenes mint az alapértelmezett csomag viszont több testreszabási lehetőséggel rendelkezik. A felminősítési folyamat megkezdéséhez kattintsunk Az oldal tetején található _Upgrade_ gombra, majd a megjelenő lapon válasszuk ki a _Pay as you go_ opciót.
![Oracle-upgrade.png](/images/Oracle-upgrade.png)
![Oracle-pay-as-you-go.png](/images/Oracle-pay-as-you-go.png)
Ezután a rendszer ismételten be fogja kérni a bankártyaadatainkat, majd végre fog hajtani egy 30.000 Ft-os tranzakciót, melyet nem sokkal később vissza is térít, ezzel megindítva a felminősítési eljárást, mely akár 2-3 napig is eltarthat.
Ha megtörtént a fiókunk felminősítése, még létre kell hoznunk egy VCN-t ([Virtual Cloud Network](https://www.oracle.com/cloud/networking/virtual-cloud-network/)) majd kimásolni az azonosítószámát, hogy meg tudjuk mondani az Oracle Supportnak, hol szeretnénk megnyittatni az SMTP portot. Ehhez látogassunk el a _Set up a network with a wizard_ menüpontba, majd kattintsunk a _Start VCN Wizard_ gombra, és adjunk meg egy nevet a VCN-ünknek majd kattintsunk a _Next_ majd a _Create_ gombra. Ha lefutott a VCN készítés folyamata, a megjelenő oldalon kattintsunk az _OCID_ felirat melletti _copy_ gombra. Ezután klikkünk a menüsoron található kérdőjel ikonra majd pedig a _Visit Support Center_ gombra. A _Support Center_-ben nyissunk egy új _Support Request_-et amiben leírjuk (természetesen angolul), hogy miért van szükségünk arra, hogy engedéyyezve legyen számunkra az SMTP porton történő kommunikációt. Fontos, hogy az üzenetünkbe mindenképpen foglaljuk bele a következőket:
- A fiókunk típusa (Esetünkben _Pay-as-you-go_.)
- A VCN-ünk azonosítója
- Pontosan milyen típusú E-maileket szeretnénk majd küldeni (magán, üzleti, stb.)
- Körülbelül mennyi adatforgalmat fog használni a szerverünk havonta (azaz mennyit tervezünk E-mailezni)

Ha mindent részletesen leírtunk, nincs más dolgunk mint várni, hogy az Oracle munkatársai elbírálják a kérelmünket. Amennyiben mindent rendben találtak pár nap elteltével meg fogják nyitni nekünk a 25-ös portot és már kezdődhet is a munka érdemi része, a szerver konfigurálása.

Első lépésként létre kell hoznunk egy új virtuális szervert. Ehhez az adminisztrációs felület kezdőlapján válasszuk a _Create a WM Instance_ menüpontot. A megjelenő lapon fontos, hogy az _Image and shape_ menüben az Ubuntu Server 18.04-es vezióját, illetve a _Shape_ menüben _Specialty and previous generation_ fül alatt található _VM.Standard.E2.1.Micro_ virtuális gépet válasszuk, mert csak így fogjuk tudni ingyenesen működtetni a szerverünket.
![Oracle-image-and-shape](/images/Oracle-image-and-shape.png)
Ezen kívül még a _Network_ fülön állítsuk át a _Primary network_-öt és a _Subnet_-et arra, amelyiken korábban kinyittattuk a az SMTP elérést. Valamint helyezzünk egy pipát a _Do not assign a public IPv4 address_ opcióhoz.
![Oracle-networking](/images/Oracle-networking.png)

Most már nincs más dolgunk, mint letölteni a rendszer által generált privát és publikus SSH kulcsot, majd a _Create_ gombra ténylegesen létrehozni a virtuális szerverünket. A megjelenő öldalon láthatjuk a szerverünk státuszát. Ha a státusz _Povisioning_-ról _Running_-ra változik, adnunk kell a gépünknek egy publikus és statikus IP címet. Ehhez a _Resources_ oszlopban kattintsunk az _Attached VNICs_ hivatkozásra majd a megjelenő listából válasszuk ki a szerverünk nevét tartalmazó _VNIC_-t. A következő lapon klikklejünk az _IPv4 address_ gombra majd az egyetlen IP címet tartalmazó sorban a "három pontos" menüben lévő _Edit_ gombra. Az így megjelent fülön állítsuk át _No pubilc IP_-ről _Reserved public IP_-ra majd tegyünk egy pipát a _Create new Reserved IP Address_ opcióhoz és adjunk meg egy tetszőleges nevet és az _Update_ gombbal mentsük a módosításainkat.
![Oracle-public-ip](/images/Oracle-public-ip.png)

{{% notice tip "Tipp"%}}
Mielőtt továbblépnénk az [MX Toolbox](https://mxtoolbox.com/blacklists.aspx) oldalán célszerű ellenőrizni, hogy a publikus IP címünk feketelistán van-e, mert akkor célszerű új IP-t generálni magunknak, mert csak egy teljesen tiszta IP cím birtokában lehetünk biztosak abban, hogy a leveleink meg fognak érkezni a címzettjeinkhez.
{{% /notice %}}

Mivel ettől kezdve rendelkezünk egy publikus IP-vel hamarosan el tudunk kezdeni dolgozni a szerveren, de előtte még meg kell nyitnunk a szükséges portokat az Oracle saját tűzfalán. A tűzfal kezeléséhez ugyanezen az oldalon ahol eddig dolgoztunk klikkeljünk a _Subnet_ után található linkre, majd a _Default Security List_ hivatkozásra. Az így elért oldalon az _Add Ingress_ Rules_ gombbal tudunk új tűzfal-szabályokat létrehozni. A képen látható beállításokat allkalmazva, nyissuk meg az alábbi portokat:
- 25 TCP (SMTP)
- 53 TCP és UDP (DNS)
- 80 TCP (HTTP)
- 443 TCP (HTTPS)
- 465 TCP (SMTP465)
- 587 TCP (SMTP submission)
- 993 TCP (IMAP)
- 995 TCP (POP)
- 4190 TCP (Sieve)
![Oracle-firewall-settings](/images/Oracle-firewall-settings.png)

Ha megnyitottuk a megfelelő portokat akkor már készen is állunk a szerveren történő munkára. Először másoljuk a korábban letöltött _.key_ kiterjesztésű fájlt a _.ssh_ mappába és a `chmod 600` paranccsal állítsuk be a megfelelő jogokat. Ez után a szerverünkbe történő belépéshez a következő parancsot fogjuk tudni használni:
```bash
ssh -i ~/.ssh/letöltött-fájl.key ubuntu@ip.cím
```
Miután beléptünk a szerverbe a `sudo -i` parancs kiadásával váljunk root felhasználóvá és az `apt update && apt upgrade` parancs segítségével frissítsük a rendszert. Miután frissült a szerver még itt is ki kell nyitnunk a szükséges portokat, mivel sajnos az Oracle és a szerverünk tűzfala nem veszi át egymás beállításait. A portok kinyitását az alábbi parancsok futtatásával tudjuk megtenni:
```bash
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 25 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 53 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p udp --dport 53 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 80 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 443 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 587 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 993 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 995 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 4190 -j ACCEPT
iptables -I INPUT 6 -m state --state NEW -p tcp --dport 465 -j ACCEPT
```
Miután lefuttattuk a fent parancsokat akkor a `netfilter-persistent save` paranccsal alkalmaznunk kell a módosításainkat.
Bár már majdnem minden telepítési feltételt teljesítettünk, még nagyon fontos hogy beállítsuk a szerverünk nevét. Ebben a `hostnamectl set-hostname` parancs lesz a segítségünkre. Fontos, hogy egy aldomaint kell használnunk akkor is, ha E-mailezésen kívül nem szeretnénk másra használni a domain nevünket (Például: `hostnamectl set-hostname mail.domainnevem.hu`) Ha beállítottuk a gépünk nevét, indítsuk újra a szerverünket.

Amint újraindult a szerver ismételten lépjünk be és vegyük fel a root jogokat a már ismert módon. Ha megvagyunk most már tényleg nincs más dolgunk mint az alábbi parancs kiadásával megkezdeni a Power Mail-in-a-Box telepítését:
```bash
curl -L https://power-mailinabox.net/setup.sh | sudo bash
```
Pár perc elteltével meg fog jelenni egy köszöntőképernyő amiről nyugodtan továbbléphetünk, ezután meg fog jelenni az alábbi képernyő ahol meg kell adnunk a szerverünk nevét, különösen figyelve az aldomain helyességére.
![Oracle-maiab-setup-dialog-1](/images/Oracle-maiab-setup-dialog-1.png)
![Oracle-maiab-setup-dialog-2](/images/Oracle-maiab-setup-dialog-2.png)
A következő párbeszédablakban létre kell hoznunk az első E-mail címünket. A @ jel után mindenképpen távolítsuk el az aldomaint!
![Oracle-maiab-setup-dialog-3](/images/Oracle-maiab-setup-dialog-3.png)
Ezek után már nincs más dolgunk mint hátradölni és várni, hogy a telepítő elvégezzen minden szükséges beállítást. Előfordulhat, hogy a telepítés közben meg kell adnunk az időzónánkat. Ha mi és a szerverünk nem ugyanabban az időzónában van, célszerű azt az időzónát megadni, ahol ténylegesen vagyunk, mert ez alapján fogja számolni a szerverünk, hogy mikor érkezett az adott E-mail. A telepítés végén meg kell adnunk az újonnan létrehozott E-mail címünkhöz egy jelszót, ami meg fog egyezni az adminisztrátori jelszóval.

Ha végzett a telepítő még be kell állítanunk, hogy a domain nevünk a szerverünk IP-címére mutasson. Mivel a Power Mail-in-a-Box DNS szerverként is működik, ezt a következő módon fogjuk tudni megtenni:
A domain nevünk regisztrátoránál állítsunk be két [Glue Record](https://www.catchpoint.com/blog/glue-records)-ot ns1.aldomain illetve ns2.aldomain néven (Például: ns1.mail.domainnevem.hu) a szerverünk IP címére mutató tartalommal, majd pedig az előbbiekben megadott két címet megadni a domainünk névszerverének. Így mostmár ha ellátogatunk a https://telepítéskor-megadott-aldomain/admin oldalra akkor be fogunk tudni lépni az adminisztrátori felületre, a telepítésnél megadott jelszóval és E-mail címmel. Ha beléptünk navigáljunk a _System_ &rarr; _TLS (SSL) Certificates_ menüpontba, ahol a _Provision Certificates_ gombra kattintva kérvényezzünk a domainünknek megbízható tanúsítványt. A szerverünk státuszát a _System_ &rarr; _Status Checks_ menüpontban tudjuk ellenőrizni. Amennyiben mindent jól csináltunk csak a _Reverse DNS_ fog piros színnel megjelenni. Ezt úgy tudjuk orvosolni, hogy a már ismert módon nyitunk egy _Support Request_-et amiben leírjuk a domainnevünket (az aldobain-nel együtt) valamint a szerverünk IP címét, és kérvényezzük a [_Reverse DNS_](https://en.wikipedia.org/wiki/Reverse_DNS_lookup) beállítását.

{{% notice info "Fontos!"%}}
Ameddig az Oracle nem állítja be a _Reverse DNS_-t **CSAK** E-mailek fogadására használjuk a szerverünket, mert ha  _Reverse DNS_ nélkül küldünk levelet, a nagyobb szolgáltatóknál könnyen feketelistára kerülhetünk!
{{% /notice %}}

Ha az Oracle beállította a _Reverse DNS_-t (A saját szerverünk is értesítést küld róla) már minden készen áll arra, hogy biztonságosan levelezzünk (A leveleinket a /mail címen érhetjük el)!

{{% notice warning "Jogi megjegyzés"%}}
Jelen cikk írója semmilyen kapcsolatban nem áll az _Oracle Corporation_-nal. A fentiekben leírtak teljes mértékben a szerző saját tapasztalatain és kutatásain alapulnak.
{{% /notice %}}
