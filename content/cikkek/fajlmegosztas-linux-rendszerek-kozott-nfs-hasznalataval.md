---
title: "Fájlmegosztas Linux rendszerek között NFS használatával"
date: 2020-06-18T09:47:16+02:00
draft: false
tags: [ "Hogyan használd" ]
image: images/NFS-Banner.png
---

Mindenki szereti biztonságos, de egyben bárhonnan elérhető helyen tartani az adatait.
Ezért szoktak sokan beruházni egy NAS-ra, pedig a legtöbb esetben a NAS kiváltható egy régebbi, de még jól működő számítógéppel is.
A saját készítésű NAS-unkon található fájlokat, pedig megoszthatjuk NFS, azaz **N**etwork **F**ile **S**ystem segítségével. Az NFS-megosztás kis rendszerigényű, így kevés memóriával rendelkező gépeken is használható.
<!--more--> 
Elsőként telepítenünk kell a szerverünkre (az a gép, ahol a megosztandó fájlok vannak) az NFS-megosztáshoz szükséges programcsomagot, amit a következő parancsok egyikének kiadásával tehetünk meg:

-   Debian/Ubuntu-alapú rendszerek esetén: `sudo apt install nfs-utils`
-   Arch-alapú rendszerek esetén: `sudo pacman -S nfs-utils`

A telepítés után, nyissuk meg szerkesztésre a /etc/exports fájlt és írjuk bele a következőt: `/home/felhasználó/megosztás 192.168.1.1/24(rw,no\_root\_squash,async)`. Azért, hogy a rendszer figyelembe vegye a változtatásainkat, ki kell adnunk a `sudo exportfs -arv` parancsot.

Ha a fent leírtakkal megvagyunk, akkor a szerverünk készen áll a fájlok megosztására, és nincsen már más hátra, mint a kliensgépek konfigurációja. Azért, hogy a gépünk lássa a megosztásunkat, telepítenünk kell egy programot, amit a következő módon tehetünk meg:

-   Debian/Ubuntu-alapú rendszerek esetén: `sudo apt install gvfs-nfs`
-   Arch-alapú rendszerek esetén: `sudo pacman -S gvfs-nfs`

Ezek után, hogyha el szeretnénk érni a megosztásunkat, akkor a fájlkezelőnk címsorába a következőt kell beírnunk: `nfs://szerver-hosztneve/megosztott-mappa/`
