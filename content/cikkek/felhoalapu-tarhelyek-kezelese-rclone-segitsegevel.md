---
title: "Felhőalapú tárhelyek kezelése Rclone segítségével"
date: 2020-06-18T09:44:26+02:00
draft: false
tags: [ "Hogyan használd" ]
image: images/Felhő-Szolgáltatások-Banner.png
---
A számítástechnika fejlődésével a tárolandó adatok mennyisége rohamosan nőni kezdett, emiatt az embereknek egyre nagyobb tárhelyekre volt szükségük. 
Viszont azzal a problémával is szembesülni kellett, hogy a tárolók mérete miatt, az adatokat csak nagyon nehezen (vagy egyáltalán nem) lehetett magunkkal hordozni. 
Éppen emiatt pár üzletember úgy gondolta, hogy azzal fog pénzt keresni, hogy odaadja szerverének egy részét más cégeknek, hogy tárolják nála az adataikat, és ezzel létrehozta az első felhőalapú tárhelyet.
Persze mára már nagyon kis méretű adattárolók is beszerezhetőek, de ettől még a felhőtárhelyek nem vesztették el létjogosultságokat, mert még mindig olcsóbbak, mint a merevlemezek.
Éppen, ezért úgy döntöttem, hogy megmutatom nektek, hogyan tudjátok kényelmesen kezelni őket Linux alatt.
Mint Linux alatt mindenre, a felhő alapú tárhelyeink kezelésére is sok lehetőség áll rendelkezésünkre.
Ebben a cikkben én most az Rclone használatát fogom bemutatni nektek, mivel majdnem minden felhőszolgáltatást támogat.
<!--more--> 
Először ugyebár fel kell telepítenünk a programot. Ezt a következő parancsok egyikével tudjuk megtenni:

- Debian/Ubuntu rendszerek alatt: `sudo apt install rclone`
- Arch rendszerek alatt pedig: `sudo pacman -S rclone`

Ahhoz, hogy a program rendesen működjön, természetesen konfigurálnunk kell. 
Az Rclone konfigurációs felületébe az rclone config parancs kiadásával tudunk belépni. 
Az n betű lenyomásával hozzunk létre egy új Rclone tárolót, és nevezzük el. Ezután a megjelenő listában keressük meg az általunk használni kívánt szolgálást, és írjuk be a számát.
A client_id és a client_secret értékeket mindig hagyjuk üresen. Következő lépésként állítsuk be, hogy mihez legyen joga az Rclonenak.
Én itt 1-est szoktam választani, azaz a programnak mindenhez joga van. 
Az Edit advanced config? kérdésre válaszoljunk nemmel, majd üssünk egy y-t az automatikus konfiguráció elindításához. 
A megnyíló böngészőablakban adjuk meg a bejelentkezési adatainkat. Ha mindent jól csináltunk, akkor egy Success! felirattal fog jutalmazni minket a rendszer.
Visszatérve a terminálunkba nyomjunk egy y és egy q betűt a konfiguráció befejezéséhez és a kilépéshez.
Az Rclonenak az a jó tulajdonsága, hogy képes felcsatolni a rendszerünkbe a tárhelyünket, úgy minthogyha az ténylegesen be lenne építve a gépünkbe, ezáltal nagyon könnyűvé téve a fájlok mozgatását a felhőnk és a gépünk között. 
A távoli tárhelyünk felcsatolásához hozzunk létre egy fájlt a `~/.config/autostart/` mappában egy .desktop kiterjesztéssel a következő tartalommal:

```bash
[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=Driveod neve ahogyan az Rcloneban megadtad
Exec=sh -c "rclone --vfs-cache-mode writes mount Driveod neve ahogyan az Rcloneban megadtad: /csatolási/pont/"
RunHook=0
StartupNotify=false
Terminal=false
Hidden=false
```
Ha mindent jól csináltunk akkor egy ki- és bejelentkezést követően egy új meghajtó fog megjelenni a fájlkezelőnkben.
